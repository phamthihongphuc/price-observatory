package demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Search_By_EAN {
	public static void main(String args[]) throws InterruptedException, IOException {
		File src = new File("/Users/phucpham/eclipse-workspace/price-observatory/demo/EAN_CODE_LIST.xlsx"); // Excel file direction.
		FileInputStream fis = new FileInputStream(src);

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("EAN_CODE_LIST");
//		System.setProperty("webdriver.chrome.driver", "C:\\SeleniumDrivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		WebDriverWait wait = new WebDriverWait(driver, 100);
		driver.get("http://www.price-observatory.com/en");
//		driver.manage().window().maximize();

		// Click login button
		driver.findElement(By.xpath("//*[@id='topBar']/div/ul[1]/li[1]")).click();

		// Enter email, password and click login button
		driver.findElement(By.xpath("//input[@type='email']")).sendKeys("phuc@price-observatory.com");
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("0JNJfFPT");
		driver.findElement(By.xpath("//*[contains(@class,'btn btn-primary')]")).click();

		// Waiting for loading page and go to account
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(@href,'/po/admin/as/158')]")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@href,'/po/admin/as/158')]")));
		driver.findElement(By.xpath("//*[contains(@href,'/po/admin/as/158')]")).click();

		// -------------------------SEARCHING PRODUCT BY EAN
		// CODE----------------------------//
		// Go to dashboard
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1)); // switches to new tab
		driver.get("http://www.price-observatory.com/po/dashboard/products");

		// Turn off Matched product tag
		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//*[contains(@id,'matchingstatusproductfilterdismiss')]")));
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//*[contains(@id,'matchingstatusproductfilterdismiss')]")));
		driver.findElement(By.xpath("//*[contains(@id,'matchingstatusproductfilterdismiss')]")).click();

		// GET LIST UNMATCHING PRODUCT
		WebElement idColumnHeader = driver.findElement(By.xpath("//*[contains(@title,'lightonline.fr')]"));
		String jqGrid = idColumnHeader.getAttribute("id");
		System.out.println("id is: " + jqGrid);

		// Get list unmatching product
		String Xpath_Of_Unmatching_Products = "//*[contains(@aria-describedby,'" + jqGrid
				+ "') and contains(@data-url, 'undefined')]";

		// Get total products
		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//*[contains(@class, 'jqgrow ui-row-ltr ui-widget-content')]")));
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//*[contains(@class, 'jqgrow ui-row-ltr ui-widget-content')]")));
		Thread.sleep(3000);
		String total_page = driver.findElement(By.xpath("//*[contains(@id, 'sp_1_jqGridPager')]")).getText();
//		System.out.println("total page: " + total_page);

		// List all unmatching product
		List<WebElement> Unmatching_Products = new ArrayList<WebElement>();
		// Create a list to contain ean code of unmatching product
		List<String> EAN_List = new ArrayList<String>();
		List<String> EAN_List_Can_Searching = new ArrayList<String>();
		List<String> EAN_List_Cannot_Searching = new ArrayList<String>();

		int current_page = 1;
		while (current_page <= Integer.parseInt(total_page)) {
			// Unmatching product per page
			java.util.List<WebElement> Unmatching_Products_Per_Page = driver
					.findElements(By.xpath(Xpath_Of_Unmatching_Products));
			System.out.println("total page: " + total_page);
			System.out.println("Current page: " + current_page);
			System.out.println("Length of list unmatching products per page: " + Unmatching_Products_Per_Page.size());
			for (int i = 0; i < Unmatching_Products_Per_Page.size(); i++) {
				System.out.println("i: " + i);
				Unmatching_Products_Per_Page.get(i).click();
				Thread.sleep(2000);
				if (driver.findElements(By.xpath("//*[@id='sproductdetailmatchingajaxtable']")).size() != 0) {
					System.out.println("There is a flag product....");
					driver.findElement(By.xpath("//*[contains(@id, 'hidecontextmenu')]")).click();
				} else {
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='productdetailtable']")));
					wait.until(
							ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='productdetailtable']")));
					String EANCode = driver.findElement(By.xpath("//*[@id=\'productdetailtable\']/tbody/tr[8]/td[2]"))
							.getText();
					System.out.println("EAN: " + EANCode);
					EAN_List.add(EANCode);
					HSSFRow row = sheet.createRow(i);
					HSSFCell cell = row.createCell(0);
					cell.setCellValue(EANCode);
					workbook.write(new FileOutputStream("EAN_CODE_LIST.xlsx"));
					driver.findElement(By.xpath("//*[contains(@class,'btn btn-default closebtn')]")).click();
				}
				Unmatching_Products_Per_Page = driver.findElements(By.xpath(Xpath_Of_Unmatching_Products));
			}
			Unmatching_Products.addAll(Unmatching_Products_Per_Page);
			System.out.println("Length of list unmatching products per page: " + Unmatching_Products_Per_Page.size());
			System.out.println("Length of ean list: " + EAN_List.size());
			System.out.println("List ean code: " + EAN_List);
			driver.findElement(By.xpath("//*[contains(@id, 'next_jqGridPager')]")).click();
			Thread.sleep(2000);
			current_page++;
		}
		System.out.println("Length of list unmatching products ehehehehhe: " + Unmatching_Products.size());
		Thread.sleep(5000);

		driver.get("https://www.lightonline.fr/");

		for (int i = 0; i < EAN_List.size(); i++) {
			WebElement search = driver.findElement(By.xpath("//*[@id=\"_search\"]"));
			search.sendKeys(EAN_List.get(i));
			search.submit();
			Thread.sleep(2000);

			if (driver.findElements(By.xpath("//*[contains(@class, 'plandusite container')]")).size() != 0) {
				System.out.println("Pass: " + EAN_List.get(i));
				EAN_List_Cannot_Searching.add(EAN_List.get(i));
			} else {
				System.out.println("Fail: " + EAN_List.get(i));
				EAN_List_Can_Searching.add(EAN_List.get(i));
			}
			Thread.sleep(1000);
		}
		System.out.println("---------------*** TEST RESULT *** ---------------");
		System.out.println("List product CAN NOT search in site: " + EAN_List_Cannot_Searching);
		System.out.println("List product CAN search in site: " + EAN_List_Can_Searching);

		driver.quit();
	}
}
